public class Knight extends Fighter {

    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    

    @Override
    public double getCombatScore() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(Utility.isSquare(Battle.GROUND))
        {
            return this.getBaseHp()*2;
        }
        else
        {
            if(this.getWp()==1)
                return (double) this.getBaseHp();
            else 
                return (double) this.getBaseHp()/10;
        }
    }
   
}
